import javax.swing.*;
import javax.swing.event.*;
import java.awt.*; 
import java.util.*;
import java.io.*;

public class Simulation extends JFrame {

  private ArrayList<Algorithm> solutions = new ArrayList<Algorithm>();
  private FunctionView function = new FunctionView(solutions);

  public Simulation() throws Exception {

    add(function);
    setSize(700, 400);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);

    boolean slowGUI = true;
    int iterations = 500;
    // String[] functs = {"P", "R"};
    // String[] algs = {"Naive", "PSO", "SA", "CS",};
    String[] functs = {"P", "R"};
    String[] algs = {"CS"};

    for (String funct : functs) {
      function.setFunction(funct);
      
      for (String alg : algs) {

        if (alg.equals("Naive")) {
          System.setOut(new PrintStream(new FileOutputStream(funct + "/" + alg + ".txt")));
          synchronized(solutions) {
            solutions.add(new AlgorithmExhaustive("Naive", function, 0, slowGUI));
          }
          for (Algorithm solution : solutions) {
            solution.start();
          }
          for (Algorithm solution : solutions) {
            solution.join();
          }
          synchronized(solutions) {
            solutions.clear();
          }
        }
        else if (alg.equals("H")) {

          int[] initialPops = {4, 5, 6, 7, 8, 9, 10, 11};
          int[] terminations = {2, 3, 4, 5, 6};

          for (int termination : terminations) {
            for (int initialPop : initialPops) {
              System.setOut(new PrintStream(new FileOutputStream(funct + "/" + alg +  "/A" + (initialPop) + "B" + (termination) + ".txt")));
              
              for (int i = 0; i < iterations; i++) {
                synchronized(solutions) {
                  solutions.add(new AlgorithmDeterministicSearch("Heuristic", function, 0, slowGUI, initialPop, termination));
                }
                for (Algorithm solution : solutions) {
                  solution.start();
                }
                for (Algorithm solution : solutions) {
                  solution.join();
                }
                synchronized(solutions) {
                  solutions.clear();
                }
              }
            }
          }
        }
        else if (alg.equals("PSO")) {

          double[] terminations = {0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0};
          double[] momentums = {0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35};

          for (double termination : terminations) {
            for (double momentum : momentums) {
              System.setOut(new PrintStream(new FileOutputStream(funct + "/" + alg +  "/A" + (int)(termination*10) + "B" + (int)(momentum*100) + ".txt")));
              
              for (int i = 0; i < iterations; i++) {
                synchronized(solutions) {
                  solutions.add(new AlgorithmParticleSwarmOptimization("PSO", function, 0, slowGUI, termination, momentum));
                }
                for (Algorithm solution : solutions) {
                  solution.start();
                }
                for (Algorithm solution : solutions) {
                  solution.join();
                }
                synchronized(solutions) {
                  solutions.clear();
                }
              }
            }
          }
        }
        else if (alg.equals("SA")) {
          double[] randomRanges = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
          double[] annScheds = {1.01, 1.04, 1.07, 1.1, 1.13, 1.16, 1.19, 1.22, 1.25, 1.28, 1.31, 1.34};

          for (double randomRange : randomRanges) {
            for (double annSched : annScheds) {
              System.setOut(new PrintStream(new FileOutputStream(funct + "/" + alg  + "/A" + (int)randomRange + "B" + (int)(annSched*100) + ".txt")));
              
              for (int i = 0; i < iterations; i++) {
                synchronized(solutions) {
                  solutions.add(new AlgorithmSimulatedAnnealing("SA", function, 0, slowGUI, randomRange, annSched, 10.0));
                }
                for (Algorithm solution : solutions) {
                  solution.start();
                }
                for (Algorithm solution : solutions) {
                  solution.join();
                }
                synchronized(solutions) {
                  solutions.clear();
                }
              }
            }
          }
        }
        else if (alg.equals("CS")) {
          int[] popSizes = {3, 4, 5, 6, 7, 8, 9};
          int[] fitSolus = {2, 3, 4, 5, 6, 7};

          for (int popSize : popSizes) {
            for (int fitSolu : fitSolus) {
              System.setOut(new PrintStream(new FileOutputStream(funct + "/" + alg  + "/A" + popSize + "B" + fitSolu + ".txt")));
              for (int i = 0; i < iterations; i++) {
                synchronized(solutions) {
                  solutions.add(new AlgorithmCuckooSearch("CS", function, 0, slowGUI, popSize, fitSolu));
                }
                for (Algorithm solution : solutions) {
                  solution.start();
                }
                for (Algorithm solution : solutions) {
                  solution.join();
                }
                synchronized(solutions) {
                  solutions.clear();
                }
              }
            }
          }
        }
        else if (alg.equals("DSA")) {
          int[] workers = {2, 3, 4, 5, 6, 7, 8};

          for (int worker : workers) {
            System.setOut(new PrintStream(new FileOutputStream(funct + "/" + alg  + "/A" + worker + "B0" + ".txt")));
            for (int i = 0; i < iterations; i++) {
              synchronized(solutions) {
                solutions.add(new AlgorithmDistributedSimulatedAnnealing("DSA", function, 0, worker));
              }
              for (Algorithm solution : solutions) {
                solution.start();
              }
              for (Algorithm solution : solutions) {
                solution.join();
              }
              synchronized(solutions) {
                solutions.clear();
              }
            }
          }
        }
        else if (alg.equals("DCS")) {
          int[] workers = {2, 3, 4, 5, 6, 7, 8};

          for (int worker : workers) {
            System.setOut(new PrintStream(new FileOutputStream(funct + "/" + alg  + "/A" + worker + "B0" + ".txt")));
            for (int i = 0; i < iterations; i++) {
              synchronized(solutions) {
                solutions.add(new AlgorithmDistributedCuckooSearch("DCS", function, 0, worker));
              }
              for (Algorithm solution : solutions) {
                solution.start();
              }
              for (Algorithm solution : solutions) {
                solution.join();
              }
              synchronized(solutions) {
                solutions.clear();
              }
            }
          }
        }
      }

      // solutions.add(new AlgorithmDeterministicSearch("Heuristic", function, 0));
      // solutions.add(new AlgorithmDistributedParticleSwarmOptimization("DPSO", function, 0));
      // solutions.add(new AlgorithmDistributedSimulatedAnnealing("DSA", function, 20));
      // solutions.add(new AlgorithmDistributedCuckooSearch("DCS", function, 40));
      // for (Algorithm solution : solutions) {
      //   solution.start();
      // }
    }
  }

  public static void main(String[] args) throws Exception {
    new Simulation();
  }
}
