import java.util.*;
import java.awt.geom.*;

class AlgorithmDeterministicSearch extends Algorithm {
  class ADCPoint implements Comparable{
    int x;
    double y;
    public ADCPoint(int x, double y){ this.x = x; this.y = y;}
    public int compareTo(Object o){
      ADCPoint p = (ADCPoint)o;
      if (this.y > p.y)  return 1;
      else if (this.y == p.y)  return 0;
      else  return -1;
    }
    public String toString(){
      return x + ", " + y;
    }
  }

  int initialPop;
  int terminationDist;
  boolean done;
  double globalBest;
  Random random;
  ArrayList<ADCPoint> pointsSearched;

  public AlgorithmDeterministicSearch(String description, FunctionView function, int displayOffset, int initialPop, int terminationDist) {
    this(description, function, displayOffset, true, initialPop, terminationDist);
  }
  
  public AlgorithmDeterministicSearch(String description, FunctionView function, int displayOffset, boolean allowDelay, int initialPop, int terminationDist) {
    super(description, function, displayOffset, allowDelay);

    this.initialPop = initialPop;
    this.terminationDist = terminationDist;
    globalBest = 0.0;
    pointsSearched = new ArrayList<ADCPoint>();

    DetCandidate dummy = new DetCandidate(0);
    random = new Random();
    //candidates.add(dummy);
  }

  
  public void run() {
    int sliceWidth = domain/initialPop;
    for (int i = 0; i < domain/sliceWidth; i++){
      int x = random.nextInt(sliceWidth) + i * sliceWidth;
      double y = getFunctionValue(x);
      pointsSearched.add(new ADCPoint(x, y));
      candidates.add(0, new DetCandidate(x));
      if (y > globalBest){
        bestCandidate = x;
        globalBest = y;
      }

      function.repaint();
    }


    while(!done){
      ArrayList<ADCPoint> topPoints = new ArrayList<ADCPoint>(pointsSearched);
      Collections.sort(topPoints, Collections.reverseOrder());

      for (int i = 0; i < topPoints.size()/2; i++){
        int q = topPoints.get(i).x;
        for (int j = 1; j < pointsSearched.size(); j++){
          int curX = pointsSearched.get(j).x;
          int prevX = pointsSearched.get(j-1).x;
          if (curX == q){
            int x = (curX+prevX)/2;
            double y = getFunctionValue(x);
            pointsSearched.add(j, new ADCPoint(x, y));

            candidates.add(0, new DetCandidate(x));
            if (y > globalBest){
              bestCandidate = x;
              globalBest = y;
            }

            function.repaint();

            if (curX-prevX < terminationDist)
              done = true;
            
            break;
          }
        }
      }
    }



    

    //p.curScore = getFunctionValue(p.location, false);
    function.repaint();
    printResult();
  }



  

}

class DetCandidate implements Candidate{
  int location;
  public DetCandidate(int location) {
    this.location = location;
  }

  public int getLocation() {
    return location;
  }

}