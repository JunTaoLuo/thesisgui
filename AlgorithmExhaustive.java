public class AlgorithmExhaustive extends Algorithm {

  double bestValue;
  static final int NUM_CANDIDATES = 1;

  public AlgorithmExhaustive(String description, FunctionView function, int displayOffset) {
    this(description, function, displayOffset, true);
  }

  public AlgorithmExhaustive(String description, FunctionView function, int displayOffset, boolean allowDelay) {
    super(description, function, displayOffset, allowDelay);

    bestValue = 0.0;

    for (int i = 0; i < NUM_CANDIDATES; i++) {
      candidates.add(new CandidateExhaustive(0));
    }
  }

  public void run() {
    boolean done = false;

    for (Candidate c : candidates) {
    }

    while (!done) {

      // System.out.println("asdfadsf ");
      for (Candidate c : candidates) {
        CandidateExhaustive candidate = (CandidateExhaustive)c;

        double newValue = getFunctionValue(candidate.location);

        if (newValue >= bestValue) {
          bestCandidate = candidate.location;
          bestValue = newValue;
        }

        function.repaint();

        if (candidate.location == domain - 1) {
          done = true;
          break;
        }

        candidate.location = (candidate.location + 1);
      }
    }

    printResult();
  }
}

class CandidateExhaustive implements Candidate {
  int location;

  public CandidateExhaustive(int startingLocation) {
    location = startingLocation;
  }

  public int getLocation() {
    return location;
  }
}