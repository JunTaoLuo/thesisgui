import java.io.*;
import java.util.*;

public class AlgorithmDistributedSimulatedAnnealing extends Algorithm {

  static final double INITIAL_TEMP = 10.0;
  static final int NUM_WORKERS = 4;
  double bestValue;
  double temperature;
  Random random;

  int lastUpdate;
  int iter;
  int outstandinWorkers;
  double oldScore;
  double newScore;
  int numWorkers;
  double slowestWorker;
  double setupTime;

  public AlgorithmDistributedSimulatedAnnealing(String description, FunctionView function, int displayOffset, int numWorkers) {
    super(description, function, displayOffset, false);

    this.numWorkers = numWorkers;
    bestValue = 0.0;
    temperature = INITIAL_TEMP;
    random = new Random();

    lastUpdate = 0;
    iter = 0;
    outstandinWorkers = 0;
    oldScore = 0.0;
    newScore = 0.0;
    setupTime = 0.0;

    initialize();
  }

  public void initialize() {

    double bestInitial = 0.0;
    int bestInitialCandidate = 0;

    for (int i = 0; i < numWorkers; i++) {
      CandidateDSA csa = new CandidateDSA(getRandomNeighbour(random.nextInt(domain), 0));
      candidates.add(csa);

      setupTime += getFunctionTime(csa.location);
      if (getFunctionValue(csa.location) > bestInitial) {
        bestInitial = getFunctionValue(csa.location);
        bestInitialCandidate = csa.location;
      }
    }

    bestCandidate = bestInitialCandidate;
    bestValue = getFunctionValue(bestCandidate);
  }

  public void run() {
    for (int i = 0; i < numWorkers; i++) {
      CandidateDSA candidate = (CandidateDSA)candidates.get(i);
      
      outstandinWorkers++;
      (new SAWorker(this, 50, getRandomNeighbour(bestCandidate, iter), candidate, 0)).start();
      
    }
  }
  
  public synchronized void processResponse(SAWorker worker, int newCandidateLocation, double score) {

    outstandinWorkers--;
    updateTemperature();
    iter++;

    newScore = score;

    if (accept(oldScore, newScore)) {
      worker.candidate.location = newCandidateLocation;
      oldScore = newScore;

      if (newScore > bestValue) {
        bestValue = newScore;
        bestCandidate = worker.candidate.location;
        lastUpdate = iter;
      }
    }

    if (!(temperature > INITIAL_TEMP/10000.0 && iter-lastUpdate < 20)) {

      slowestWorker = Math.max(slowestWorker, worker.workerTime);

      if (outstandinWorkers == 0) {
        printDistributedResult(slowestWorker + setupTime);
        return;
      }
      return;
    }

    outstandinWorkers++;
    (new SAWorker(this, worker.delay, getRandomNeighbour(bestCandidate, iter), worker.candidate, worker.workerTime)).start();

    function.repaint();
  }

  public void updateTemperature(){
    temperature /= 1.04;
  }

  public int getRandomNeighbour(int curQ, int iter){

    // double randomChance = 1/Math.log(iter + 2);

    if (random.nextDouble() < 0.5) {
      double range = domain;
      range = (range-5)*Math.exp(-iter/7) + 5;

      if (random.nextDouble() > 0.5)
        return Math.min(Math.max(curQ + (random.nextInt((int)range)+1), 0), domain-1);
      else
        return Math.min(Math.max(curQ - (random.nextInt((int)range)+1), 0), domain-1);
    }
    else
      return random.nextInt(domain);
  }

  public boolean accept(double oldScore, double newScore){
    if (newScore > oldScore) return true;
    else {
      return Math.exp((newScore-oldScore)/temperature) > random.nextDouble();
    }
  }
}

class SAWorker extends Thread {

  AlgorithmDistributedSimulatedAnnealing responder;
  int delay;
  int newCandidateLocation;
  CandidateDSA candidate;
  double workerTime;

  public SAWorker(AlgorithmDistributedSimulatedAnnealing algo, int delay, int newCandidateLocation, CandidateDSA candidate, double workerTime) {
    responder = algo;
    this.delay = delay;
    this.candidate = candidate;
    this.newCandidateLocation = newCandidateLocation;
    this.workerTime = workerTime;
  }

  public void run() {
    try {
      Thread.sleep(delay);
    } catch (Exception e) {
    }

    workerTime += responder.getFunctionTime(newCandidateLocation);
    responder.processResponse(this, newCandidateLocation, responder.getFunctionValue(newCandidateLocation));
  }
}

class CandidateDSA implements Candidate {
  int location;

  public CandidateDSA(int startingLocation) {
    location = startingLocation;
  }

  public int getLocation() {
    return location;
  }
}