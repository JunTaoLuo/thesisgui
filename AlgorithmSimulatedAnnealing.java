import java.io.*;
import java.util.*;

public class AlgorithmSimulatedAnnealing extends Algorithm {

  double bestValue;
  SAUtils SA;

  public AlgorithmSimulatedAnnealing(String description, FunctionView function, int displayOffset, double randomRange, double annealingSchedule, double initialTemp) {
    this(description, function, displayOffset, true, randomRange, annealingSchedule, initialTemp);
  }

  public AlgorithmSimulatedAnnealing(String description, FunctionView function, int displayOffset, boolean allowDelay, double randomRange, double annealingSchedule, double initialTemp) {
    super(description, function, displayOffset, allowDelay);

    bestValue = 0.0;
    SA = new SAUtils(0, this.domain-1, randomRange, annealingSchedule, initialTemp);

    CandidateSA csa = new CandidateSA(SA.getRandomNeighbour(SA.random.nextInt(SA.maxQ - SA.minQ) + SA.minQ, 0));
    candidates.add(csa);
    bestCandidate = csa.location;
  }

  public void run() {

    int lastUpdate = 0;
    int iter = 0;
    double oldScore = 0;
    double newScore = oldScore;

    while (SA.temperature > SA.initialTemp/10000.0 && iter-lastUpdate < 20){

      for (Candidate c : candidates) {
        CandidateSA candidate = (CandidateSA)c;

        SA.updateTemperature(iter);
      
        int newCandidate = SA.getRandomNeighbour(bestCandidate, iter);
        newScore = getFunctionValue(newCandidate);

        if (SA.accept(oldScore, newScore)){
          candidate.location = newCandidate;
          oldScore = newScore;
          if (newScore > bestValue){
            bestValue = newScore;
            bestCandidate = candidate.location;
            lastUpdate = iter;
          }
        }
        iter++;
        function.repaint();
      }
    }

    printResult();
  }
}

class SAUtils {
  double initialTemp, randomRange, temperature, annealingSchedule;
  Random random;
  int minQ, maxQ;
  
  ArrayList<Integer> Qs;

  public SAUtils(int minQ, int maxQ, double randomRange, double annealingSchedule, double initialTemp){
    this.initialTemp = initialTemp;
    this.randomRange = randomRange;
    this.annealingSchedule = annealingSchedule;
    this.temperature = this.initialTemp;
    this.random = new Random();
    
    this.minQ = minQ;
    this.maxQ = maxQ;

    this.Qs = new ArrayList<>();
    for (int i = minQ; i < maxQ; i++)
      this.Qs.add(i);
  }
  
  public void updateTemperature(int iter){
    this.temperature /= annealingSchedule;
  }

  public int getRandomNeighbour(int curQ, int iter){
    
    if (this.random.nextDouble() > 0.5){
      double range = this.maxQ - this.minQ;
      range = (range-5)*Math.exp(-iter/randomRange) + 5;

      if (this.random.nextDouble() > 0.5)
        return Math.min(Math.max(curQ + (this.random.nextInt((int)range)+1),this.minQ), this.maxQ);
      else
        return Math.min(Math.max(curQ - (this.random.nextInt((int)range)+1),this.minQ), this.maxQ);
    }
    else
      return this.random.nextInt(this.maxQ - this.minQ) + this.minQ;
  }

  public boolean accept(double oldScore, double newScore){
    if (newScore > oldScore)    return true;
    else{
      return Math.exp((newScore-oldScore)/this.temperature) > random.nextDouble();
    }
  }
}


class CandidateSA implements Candidate {
  int location;

  public CandidateSA(int startingLocation) {
    location = startingLocation;
  }

  public int getLocation() {
    return location;
  }
}