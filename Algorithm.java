import java.util.*;

public abstract class Algorithm extends Thread{
  int domain;
  int bestCandidate;
  int displayOffset;
  int evaluationCount;
  double evaluationTime;
  boolean allowDelay;

  String description;
  FunctionView function;
  List<Candidate> candidates;
  HashMap<Integer, Double> functionValues;

  public Algorithm(String description, FunctionView function, int displayOffset, boolean allowDelay) {
    this.description = description;
    this.domain = function.getDomain();
    this.displayOffset = displayOffset;
    this.evaluationCount = 0;
    this.evaluationTime = 0.0;
    this.allowDelay = allowDelay;

    this.function = function;
    this.candidates = new ArrayList<Candidate>();
    this.functionValues = new HashMap<Integer, Double>();
  }

  public double getFunctionTime(int location) {
    if (functionValues.containsKey(location)) {
      return 0;
    }
    
    return function.getTime(location);
  }

  public double getFunctionValue(int location) {
    if (functionValues.containsKey(location)) {
      return functionValues.get(location);
    }

    evaluationCount++;
    evaluationTime += function.getTime(location);
    functionValues.put(location, function.getValue(location));
    
    if (allowDelay) {
      try { Thread.sleep(100); } catch (Exception e) {}
    }

    return function.getValue(location);
  }

  public void printResult() {
    System.out.println(description + " found a solution " 
      + getFunctionValue(bestCandidate) +  " using " 
      + evaluationCount + " evaluations taking " + evaluationTime);
  }

  public void printDistributedResult(double slowestThread) {
    System.out.println(description + " found a solution " 
      + getFunctionValue(bestCandidate) +  " using " 
      + evaluationCount + " evaluations taking " + slowestThread);
  }

  public abstract void run();

}

interface Candidate {
  public int getLocation();
}