import java.io.*;
import java.util.*;

class AlgorithmCuckooSearch extends Algorithm {
  int popSize;
  int numFitSolutions;
  int minQ, maxQ;
  Random random;
  HashSet<Integer> toBeExplored;

  public AlgorithmCuckooSearch(String description, FunctionView function, int displayOffset, int popSize, int fitSolutions) {
    this(description, function, displayOffset, true, popSize, fitSolutions);
  }

  public AlgorithmCuckooSearch(String description, FunctionView function, int displayOffset, boolean allowDelay, int popSize, int fitSolutions) {
    super(description, function, displayOffset, allowDelay);

    this.popSize = popSize;
    this.numFitSolutions = fitSolutions;
  }

  public void initialize(int minQ, int maxQ, int popSize, int numFitSolutions){
    this.random = new Random();
    this.minQ = minQ;
    this.maxQ = maxQ;
    this.popSize = popSize;
    this.numFitSolutions = numFitSolutions;

    this.toBeExplored = new HashSet<Integer>();
    for (int i = minQ; i < maxQ; i++)
      this.toBeExplored.add(i);
    
    // System.out.println("\n\n CS"); 

    for (int i = 0; i < popSize; i++){
      int q;
      do {
        q = random.nextInt(maxQ - minQ) + minQ;
      } while (!toBeExplored.contains(q));
      candidates.add(new Solution(q, getFunctionValue(q)));
      this.toBeExplored.remove(q);
      // System.out.println(q);
    }

    Collections.sort(candidates,Collections.reverseOrder());
  }

  public void modifySolutions(){
    boolean eliteChanged = false;
    for (int i = 0; i < numFitSolutions; i++){
            //System.out.println(numFitSolutions);
      Solution s = (Solution)(candidates.get(i));

      if(toBeExplored.isEmpty()){
        System.out.println("NO MORE SOLUTIONS!");
        return;
      }

      int step = 1;
      int newQ = s.q;
      Solution tempSolution;
      while(true){
        int tempStep = step;
        if (random.nextDouble() > 0.5)
          tempStep = step*-1;

        if (toBeExplored.contains(s.q + step)){
          tempSolution = new Solution(s.q + step, getFunctionValue(s.q+step));
          break;
        } else if (toBeExplored.contains(s.q - step)){
          tempSolution = new Solution(s.q - step, getFunctionValue(s.q-step));
          break;
        }
        step += 1;
      }

      toBeExplored.remove(tempSolution.q);
      if (tempSolution.score > ((Solution)candidates.get(i)).score){
        candidates.set(i, tempSolution);
        function.repaint();
      }
    }


    for (int i = numFitSolutions; i < popSize; i++){
      int q;
      do {
        q = random.nextInt(maxQ - minQ) + minQ;
      } while (!toBeExplored.contains(q));
      candidates.set(i, new Solution(q, getFunctionValue(q)));
      function.repaint();

      this.toBeExplored.remove(q);
    }

    Collections.sort(candidates,Collections.reverseOrder());
  }


  public void run() {
    initialize(0, domain-1, popSize, numFitSolutions);
    
    if (numFitSolutions >= popSize) {
      printResult();
      return;
    }

    Solution best = (Solution)candidates.get(0);
    double bestScore = best.score;
    int bestIter = 0;

    for (int i = 0; i < 200; i++){
      modifySolutions();
      
      best = (Solution)candidates.get(0);
      bestCandidate = best.q;
      
      if (best.score > bestScore){
        bestScore = best.score;
        bestIter = i;
      }

      if (i-bestIter >= 4)
        break;
    }
    printResult();
  }
}

class Solution implements Candidate, Comparable{
  int q;
  double score;

  public Solution(int q, double score){
    this.q = q;
    this.score = score;
  }

  public int compareTo(Object o){
    Solution other = (Solution)o;
    if (this.score > other.score)   return 1;
    else if (this.score == other.score) return 0;
    else    return -1;
  }

  public String toString(){
    return this.q + ": " + this.score;
  }

  public int getLocation() {
    return q;
  }
}