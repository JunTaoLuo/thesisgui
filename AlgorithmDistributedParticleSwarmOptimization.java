import java.util.*;

class AlgorithmDistributedParticleSwarmOptimization extends Algorithm {

  static final int NUM_CANDIDATES = 4;
  static final int MAX_ITER = 20;
  static final double TERMINATION_SPEED = 2.0;
  double cP = 0.2;
  double cG = 0.4;
  double globalBest;
  HashMap<Integer, Double> computedValues;

  Random rng = new Random();
  long startTime = System.currentTimeMillis();
  int effectiveTime = 0;

  public AlgorithmDistributedParticleSwarmOptimization(String description, FunctionView function, int displayOffset) {
    super(description, function, displayOffset, false);

    globalBest = 0.0;

    initializeSwarm();
  }

  public void initializeSwarm() {
    computedValues = new HashMap<Integer, Double>();
    for (int i = 0; i < NUM_CANDIDATES; i++) {
      Particle particle = new Particle((i+1)*10);
      //Particle particle = new Particle(1000);
      //particle.location = rng.nextInt(domain-1);
      particle.location = rng.nextInt(domain/NUM_CANDIDATES*i+1);
      particle.velocity = rng.nextDouble()*2-1;
      
      candidates.add(particle);
    }

    for (int i = 0; i < NUM_CANDIDATES; i++){
      Particle p = (Particle)candidates.get(i);
      ParticleWorker pw = new ParticleWorker(p);
      pw.start();
    }

    while(true){
      try{Thread.sleep(1);} catch(Exception e){}

      boolean allDone = true;
      for (int i = 0; i < NUM_CANDIDATES; i++){
        Particle p = (Particle)candidates.get(i);
        if (p.computing)
          allDone = false;
      }
      if (allDone)
        break;
    }

    for (int i = 0; i < NUM_CANDIDATES; i++){
      Particle p = (Particle)candidates.get(i);
      p.particleBest = p.curScore;
      p.particleBestLocation = p.location;
    }
  }

  public void run() {
    int time = 0;
    boolean done = false;

    // for (int i = 0; i < NUM_CANDIDATES; i++){
    //   Particle p = (Particle)candidates.get(i);
    //   ParticleWorker pw = new ParticleWorker(p);
    //   pw.start();
    // }
    // try{Thread.sleep(10);}catch(Exception e){}
    long maxProcTimeMs = 0;
    for (int i = 0; i < NUM_CANDIDATES; i++){
      Particle p = (Particle)candidates.get(i);
      if (p.timeLived > maxProcTimeMs)
        maxProcTimeMs = p.timeLived;
    }

    double maxEfficiency = 0;

    int lastTimeUpdated = 0;

    while (/*time < MAX_ITER &&*/ !done) {
      double w = 0.8;//(double)(MAX_ITER-time)/(MAX_ITER);

      double maxSpeed = 0;
      
      // update best candidates
      for (Candidate c : candidates) {
        Particle particle = (Particle)c;

        if(!particle.computing){
          
          if (particle.timeLived > 2*maxProcTimeMs)
            maxEfficiency = Math.max(maxEfficiency, particle.cumScore/particle.timeLived);

          computedValues.put(particle.location, particle.curScore);
          // update particleBest
          if (particle.curScore > particle.particleBest) {
            particle.particleBest = particle.curScore;
            particle.particleBestLocation = particle.location;
          }

          // update globalBest
          if (particle.particleBest > globalBest) {
            lastTimeUpdated = time;
            globalBest = particle.particleBest;
            bestCandidate = particle.particleBestLocation;
          }

          double maxV = 0;
          for (Candidate can : candidates) {
            Particle part = (Particle)can;
            maxV = Math.max(maxV, Math.abs(part.velocity));
          }

          // if (time - lastTimeUpdated > NUM_CANDIDATES*5 || (time > 10 && maxV < 0.5)){
          //   System.out.println(time);
          //   System.out.println(lastTimeUpdated);
          //   printResult();
          //   System.out.println(System.currentTimeMillis() - startTime);
          //   System.out.println(effectiveTime);
          //   return;
          // }

          double mP = rng.nextDouble() * cP;
          double mG = rng.nextDouble() * cG;

          // update velocity
          particle.velocity = w * particle.velocity + mP * (particle.particleBestLocation - particle.location) + mG * (bestCandidate - particle.location);

          // update location
          // if (particle.velocity > 1)
          //   System.out.println("particle " + particle.msDelay + " is done computing! time lived: " + particle.timeLived + " " + particle.cumScore/particle.timeLived);

          particle.location = Math.min(Math.max(0, particle.location + (int)particle.velocity), domain-1);


          if (!computedValues.containsKey(particle.location)){

            //if (particle.timeLived > 2*maxProcTimeMs && particle.cumScore/particle.timeLived < 0.8*maxEfficiency){
            if (particle.msDelay > 0.5*maxProcTimeMs && particle.cumScore/particle.timeLived < 0.5*maxEfficiency){
              // particle.location = rng.nextInt(domain-1);
              // particle.location = bestCandidate+rng.nextInt((domain-1)/5)-(domain-1)/10;
              particle.location = bestCandidate+rng.nextInt(10)-5;
              particle.location = Math.min(Math.max(0, particle.location), domain-1);
              
              particle.particleBestLocation = particle.location;
              particle.velocity = rng.nextDouble()*2-1;
              // System.out.println("RESTARTTINGGGG! particle " + particle.msDelay);
            }


            ParticleWorker pw = new ParticleWorker(particle);
            pw.start();
            try{Thread.sleep(10);}catch(Exception e){}
            // System.out.println("computing...");
            time++;
          } else {
            particle.curScore = computedValues.get(particle.location);
          }

          for (int i = 0; i < NUM_CANDIDATES; i++){
            Particle p = (Particle)candidates.get(i);
            maxSpeed = Math.max(maxSpeed, Math.abs(p.velocity));
          }
          
          if (time > NUM_CANDIDATES*1 && maxSpeed < TERMINATION_SPEED){
            System.out.print(effectiveTime + ", ");
            printResult();
            //System.out.println(System.currentTimeMillis() - startTime);
            //System.out.println(effectiveTime);
            return;
          }


          function.repaint();
      
        }
      }

      // done = 
      // System.out.println(maxLocation);
      // System.out.println(minLocation + "\n");
      // System.out.println(done);
      

      //time++;
    }

    printResult();
  }



  class ParticleWorker extends Thread{
    Particle p;
    public ParticleWorker(Particle p){
      this.p = p;
    }

    public void run(){
      p.computing = true;
      p.curScore = getFunctionValue(p.location);
      try{
        Thread.sleep(p.msDelay);
      } catch (Exception e){}
      p.timeLived = System.currentTimeMillis() - p.startTime;
      p.cumScore += p.curScore;
      p.computing = false;
      effectiveTime += p.msDelay;
    }

  }
}

class Particle implements Candidate{
  long startTime, timeLived;
  double particleBest = 0.0;
  int particleBestLocation = 0;
  double curScore = 0.0;
  double cumScore = 0.0;
  double velocity;
  int location;

  int msDelay;
  boolean computing = false;


  public Particle(int delay) {
    this.msDelay = delay;
    startTime = System.currentTimeMillis();
    timeLived = 0;
  }

  public int getLocation() {
    return location;
  }

}