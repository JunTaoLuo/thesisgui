import javax.swing.*;
import javax.swing.event.*;
import java.awt.*; 
import java.util.*;

public class FunctionView extends JPanel {

  //Scalefactor manipulates the number of domain on the screen 
  //Joining these domain forms our wave. It will represent the clarity 
  //of the display but low values may distort the wave
  private static final int SCALEFACTOR = 200;
  //The number of complete waves to display on the screen
  private static final int CYCLES = 4;
  //Total number of domain that will be joined to form the wave
  private int domain;

  //The actual sine values calculated. Drawing will be done relative to
  //the height (actual vertical coordinates calculated from sine values). These aren't used as some coordinates.
  private double[][] function;
  private double range;
  private double mean;

  private ArrayList<Algorithm> solutions;

  public FunctionView(ArrayList<Algorithm> solutions) {
    //Constructor Sets the number of cycles to display on the screen
    this.solutions = solutions;
  }

  public int getDomain() {
    return domain;
  }

  public double getValue(int point) {
    return function[point][0];
  }

  public double getTime(int point) {
    return function[point][1];
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    //The current width of the Panel
    int maxWidth = getWidth();
    //hstep mean the horizontal distance between two domain on the screen
    double hstep = (double) maxWidth / (double) domain;
    //The maximum height
    int maxHeight = getHeight();

    int[] scaledPts = new int[domain];
    for (int i = 0; i < domain; ++i) {
      scaledPts[i] = (int)(((function[i][0] - mean) / range) * maxHeight * 0.75 + maxHeight / 2);
      // System.out.println(scaledPts[i]);
    }

    g.setColor(Color.RED);
    for (int i = 1; i < domain; ++i) {

      int x1 = (int) ((i - 1) * hstep);
      int x2 = (int) (i * hstep);

      //The vertical values have already been calculated
      int y1 = maxHeight - scaledPts[i - 1];
      int y2 = maxHeight - scaledPts[i];

      //Now draw a line based on these calculated coordinates based on sine values
      g.drawLine(x1, y1, x2, y2);
    }

    synchronized(solutions) {
      for (Algorithm solution : solutions) {
        // draw current
        g.setColor(Color.BLACK);
        for (int i = 0; i < solution.candidates.size(); i++){
          Candidate c = solution.candidates.get(i);
        //for (Candidate c : solution.candidates) {
          g.drawLine((int)(c.getLocation() * hstep), 0, (int)(c.getLocation() * hstep), maxHeight);
          g.drawString(solution.description+""+i, (int)(c.getLocation() * hstep), maxHeight - solution.displayOffset);
        }

        // draw best
        g.setColor(Color.GREEN);
        g.drawLine((int)(solution.bestCandidate * hstep), 0, (int)(solution.bestCandidate * hstep), maxHeight);
        g.drawString(solution.description, (int)(solution.bestCandidate * hstep), maxHeight - solution.displayOffset - 10);
      }
    }
  }

  public void setFunction(String functionSelect) {
    domain = SCALEFACTOR * CYCLES * 2;

    // function = new double[domain];
    // double[] sine1 = new double[domain];
    // double[] sine2 = new double[domain];
    // double[] sine3 = new double[domain];
    // double[] sine4 = new double[domain];

    // for (int i = 0; i < domain; ++i) {

    //   double radians = (Math.PI / SCALEFACTOR) * i; 

    //   sine1[i] = Math.sin(0.25 * radians);
    //   sine2[i] = Math.sin(radians);
    //   sine3[i] = Math.sin(7 * radians);
    //   sine4[i] = Math.sin(19 * radians);

    //   //Now calculate the actual sin values based on radians
    //   function[i] = 0.5 * sine1[i] + sine2[i] + 0.5 * sine3[i] + 0.25 * sine4[i];
    // }

    domain = Data.DOMAIN;
    if (functionSelect.equals("P"))
      function = Data.P;
    else if (functionSelect.equals("R"))
      function = Data.R;
    double min = Double.MAX_VALUE;
    double max = Double.MIN_VALUE;
    // double sum = 0.0;

    for (int i = 0; i < domain; i++) {
      double value = function[i][0];
      min = Math.min(min, value);
      max = Math.max(max, value);
      // sum += value;
    }
    range = max-min;
    mean = (max+min)/2;

    repaint();
  }
}