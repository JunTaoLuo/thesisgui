import java.util.*;

class AlgorithmParticleSwarmOptimization extends Algorithm {

  static final int NUM_CANDIDATES = 4;
  static final int MAX_ITER = 20;
  double terminationSpeed;
  double cP;
  double cG;
  double globalBest;

  Random rng = new Random();
  HashMap<Integer, Double> computedValues;

  public AlgorithmParticleSwarmOptimization(String description, FunctionView function, int displayOffset, double termination, double momentum) {
    this(description, function, displayOffset, true, termination, momentum);
  }

  public AlgorithmParticleSwarmOptimization(String description, FunctionView function, int displayOffset, boolean allowDelay,  double termination, double momentum) {
    super(description, function, displayOffset, allowDelay);

    globalBest = 0.0;
    this.cP = momentum;
    this.cG = 2*momentum;
    this.terminationSpeed = termination;

    initializeSwarm();
  }

  public void initializeSwarm() {
    computedValues = new HashMap<Integer, Double>();
    for (int i = 0; i < NUM_CANDIDATES; i++) {
      Particle particle = new Particle();
      particle.location = domain*i/NUM_CANDIDATES;
      particle.velocity = rng.nextDouble()*2-1;
      particle.particleBest = getFunctionValue(particle.location);
      particle.particleBestLocation = particle.location;
      candidates.add(particle);
    }
  }

  public void run() {
    int time = 0;
    boolean done = false;

    while (time < MAX_ITER && !done) {
      double w = (double)(MAX_ITER-time)/(MAX_ITER);

      double maxSpeed = 0.0;
      
      // update best candidates
      for (Candidate c : candidates) {
        Particle particle = (Particle)c;

        double newValue;
        if (!computedValues.containsKey(particle.location)){
          newValue = getFunctionValue(particle.location);
          computedValues.put(particle.location, newValue);
        }
        else {
          newValue = computedValues.get(particle.location);
        }

        // update particleBest
        if (newValue > particle.particleBest) {
          particle.particleBest = newValue;
          particle.particleBestLocation = particle.location;
        }

        // update globalBest
        if (particle.particleBest > globalBest) {
          globalBest = particle.particleBest;
          bestCandidate = particle.particleBestLocation;
        }

        double mP = rng.nextDouble() * cP;
        double mG = rng.nextDouble() * cG;

        // update velocity
        particle.velocity = w * particle.velocity + mP * (particle.particleBestLocation - particle.location) + mG * (bestCandidate - particle.location);

        // update location
        particle.location = Math.min(Math.max(0, particle.location + (int)particle.velocity), domain-1);

        maxSpeed = Math.max(maxSpeed, Math.abs(particle.velocity));

        function.repaint();
      }

      done = maxSpeed < terminationSpeed;

      time++;
    }

    printResult();
  }
}

class Particle implements Candidate{

  double particleBest = 0.0;
  int particleBestLocation = 0;
  double velocity;
  int location;

  public Particle() {
  }

  public int getLocation() {
    return location;
  }
}