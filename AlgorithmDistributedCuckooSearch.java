import java.io.*;
import java.util.*;

class AlgorithmDistributedCuckooSearch extends Algorithm {
  static final int POP_SIZE = 5;
  static final int NUM_FIT = 3;
  static final int ITER_MAX = 200;
  static final int ITER_TERMINATION = 4;
  static final int NUM_WORKERS = 4;

  Random random;
  int currentIter;
  int bestIter;
  int outstandingWorkers;
  DCSSolution best;
  HashSet<Integer> toBeExplored;
  int numWorkers;
  double slowestWorker;
  double setupTime;

  public AlgorithmDistributedCuckooSearch(String description, FunctionView function, int displayOffset, int numWorkers) {
    super(description, function, displayOffset, false);

    this.numWorkers = numWorkers;
    setupTime = 0.0;
    slowestWorker = 0.0;
  }

  public void initialize(){
    random = new Random();
    currentIter = 0;
    outstandingWorkers = 0;

    this.toBeExplored = new HashSet<Integer>();
    for (int i = 0; i < domain; i++)
      this.toBeExplored.add(i);
    
    for (int i = 0; i < POP_SIZE; i++){
      int q;
      do {
        q = random.nextInt(domain);
      } while (!toBeExplored.contains(q));
      setupTime += getFunctionTime(q);
      candidates.add(new DCSSolution(q, getFunctionValue(q)));
      this.toBeExplored.remove(q);
    }

    Collections.sort(candidates, Collections.reverseOrder());
    best = (DCSSolution)candidates.get(0);
  }

  public void run() {
    initialize();

    for (int i = 0; i < numWorkers; i++) {
      outstandingWorkers++;
      (new CSWorker(this, 50, candidates, toBeExplored, 0)).start();
    }
  }

  public synchronized void processResponse(CSWorker worker, ArrayList<Candidate> updatedCandidates) {
    currentIter++;
    outstandingWorkers--;

    if (!(currentIter < ITER_MAX && currentIter - bestIter <= ITER_TERMINATION)) {
      slowestWorker = Math.max(slowestWorker, worker.workerTime);

      if (outstandingWorkers == 0) {
        printDistributedResult(slowestWorker + setupTime);
        return;
      }
      return;
    }

    // update current candidates based on updatedCandidates
    candidates.addAll(updatedCandidates);
    Collections.sort(candidates, Collections.reverseOrder());
    candidates = candidates.subList(0, POP_SIZE);
      
    DCSSolution newBest = (DCSSolution)candidates.get(0);
    int newBestCandidate = newBest.q;

    if (newBest.score > best.score){
      best = newBest;
      bestCandidate = newBestCandidate;
      bestIter = currentIter;
    }

    // send off a new job
    outstandingWorkers++;
    (new CSWorker(this, worker.delay, candidates, toBeExplored, worker.workerTime)).start();

    function.repaint();
  }
}

class CSWorker extends Thread {
  AlgorithmDistributedCuckooSearch responder;
  ArrayList<Candidate> localCandidates;
  HashSet<Integer> toBeExplored;
  int delay;
  double workerTime;

  public CSWorker(AlgorithmDistributedCuckooSearch algo, int delay, List<Candidate> candidates, HashSet<Integer> toBeExplored, double workerTime) {
    responder = algo;
    this.delay = delay;
    this.toBeExplored = toBeExplored;
    this.workerTime = workerTime;
    
    localCandidates = new ArrayList<Candidate>();
    for (Candidate c : candidates) {
      localCandidates.add(new DCSSolution((DCSSolution)c));
    }
  }

  public void run() {
    for (int i = 0; i < responder.NUM_FIT; i++) {
      if(toBeExplored.isEmpty()){
        System.out.println("NO MORE DCSSolutionS!");
        return;
      }

      DCSSolution s = (DCSSolution)(localCandidates.get(i));

      int step = 1;
      int newQ = s.q;
      DCSSolution tempDCSSolution;

      while(true){
        int tempStep = step;
        if (responder.random.nextDouble() > 0.5)
          tempStep = step*-1;

        if (toBeExplored.contains(s.q + step)){
          workerTime += responder.getFunctionTime(s.q+step);
          tempDCSSolution = new DCSSolution(s.q + step, responder.getFunctionValue(s.q+step));
          break;
        } else if (toBeExplored.contains(s.q - step)){
          workerTime += responder.getFunctionTime(s.q-step);
          tempDCSSolution = new DCSSolution(s.q - step, responder.getFunctionValue(s.q-step));
          break;
        }
        step += 1;
      }

      toBeExplored.remove(tempDCSSolution.q);
      
      if (tempDCSSolution.score > ((DCSSolution)localCandidates.get(i)).score){
        localCandidates.set(i, tempDCSSolution);
      }
    }

    for (int i = responder.NUM_FIT; i < responder.POP_SIZE; i++){
      int q;
      do {
        q = responder.random.nextInt(responder.domain);
      } while (!toBeExplored.contains(q));

      workerTime += responder.getFunctionTime(q);
      localCandidates.set(i, new DCSSolution(q, responder.getFunctionValue(q)));

      toBeExplored.remove(q);
    }

    Collections.sort(localCandidates,Collections.reverseOrder());

    try {
      Thread.sleep(delay);
    } catch (Exception e) {
    }

    responder.processResponse(this, localCandidates);
  }
}

class DCSSolution implements Candidate, Comparable{
  int q;
  double score;

  public DCSSolution(DCSSolution s) {
    this(s.q, s.score);
  }

  public DCSSolution(int q, double score){
    this.q = q;
    this.score = score;
  }

  public int compareTo(Object o){
    DCSSolution other = (DCSSolution)o;
    if (this.score > other.score)   return 1;
    else if (this.score == other.score) return 0;
    else    return -1;
  }

  public String toString(){
    return this.q + ": " + this.score;
  }

  public int getLocation() {
    return q;
  }
}
